from flask import Flask, render_template, url_for
from datetime import datetime
import platform
import sys

app = Flask(__name__)


def get_data():
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    os_info = platform.platform()
    python_version = sys.version_info[0]
    return [current_time, os_info, python_version]


# creating our routes
@app.route('/')
def index():
    data = get_data()
    return render_template('index.html', time=data[0], os_info=data[1], python_version=data[2])


@app.route('/about')
def about():
    data = get_data()
    return render_template('about.html', time=data[0], os_info=data[1], python_version=data[2])


@app.route('/tasks')
def tasks():
    movies = ['The Shawshank Redemption',
              'The Godfather',
              'The Dark Knight',
              'The Lord of the Rings: The Return of the King',
              'Pulp Fiction',
              'Forrest Gump',
              'Inception',
              'The Matrix',
              'Goodfellas',
              'The Silence of the Lambs'
              ]
    data = get_data()
    return render_template('tasks.html', time=data[0], os_info=data[1], python_version=data[2], movies=movies)


# run flask app
if __name__ == "__main__":
    app.run(debug=True)
