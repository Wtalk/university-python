from .. import db
import enum


class FormsStudyType(enum.Enum):
    Eye = 'Eye'
    Correspondence = 'Correspondence'
    Dual = 'Dual'


class Reader(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(30), unique=False, nullable=False)
    last_name = db.Column(db.String(30), unique=False, nullable=False)
    form_study_type = db.Column(db.Enum(FormsStudyType))
    email = db.Column(db.String(40), unique=False, nullable=False)
    book_title = db.Column(db.String(50), unique=False, nullable=False)
    date_issue = db.Column(db.DateTime, default=db.func.now())

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
