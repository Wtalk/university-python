from flask import Blueprint

library_blueprint = Blueprint('lib', __name__, template_folder="templates/library_reader")

from . import views