from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.validators import InputRequired, Length, Regexp


class ReaderFormCreate(FlaskForm):
    first_name = StringField('First name', validators=[InputRequired('A first name is required'),
                                                     Length(min=1, max=25, message='Name must have greater 4 '
                                                                                   'symbol and least 25 symbol'),
                                                     Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                            'Username must have only letters, numbers, '
                                                            'dots or underscores')])

    last_name = StringField('Last name', validators=[InputRequired('A last name is required'),
                                                    Length(min=1, max=25, message='Name must have greater 4 '
                                                                                  'symbol and least 25 symbol'),
                                                    Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                                           'Username must have only letters, numbers, '
                                                           'dots or underscores')])

    email = StringField('Email', validators=[InputRequired('Email is required'),
                                             Regexp('^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$',
                                                    message='Invalid Email')])

    form_study_type = SelectField('Form study type',
                                  choices=[('Eye', 'Eye'), ('Correspondence', 'Correspondence'), ('Dual', 'Dual')])

    book_title = StringField('Book title', validators=[InputRequired(), Length(min=2, max=60)])
