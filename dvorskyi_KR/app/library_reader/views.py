from flask import redirect, render_template, url_for, flash, abort
from . import library_blueprint
from flask_login import login_required, current_user, login_user, logout_user
from .. import db
from .forms import ReaderFormCreate
from .models import Reader


@library_blueprint.route("/create", methods=['GET', 'POST'])
@login_required
def create_reader():
    form = ReaderFormCreate()
    if form.validate_on_submit():
        reader = Reader(first_name=form.first_name.data, last_name=form.last_name.data,
                        form_study_type=form.form_study_type.data, email=form.email.data,
                        book_title=form.book_title.data, user_id=current_user.id)

        db.session.add(reader)
        db.session.commit()

        return redirect(url_for('lib.detail_reader', id=reader.id))

    return render_template('form_readers.html', form=form)


@library_blueprint.route("/", methods=['GET', 'POST'])
def readers():
    readers_lib = Reader.query.all()
    return render_template('readers.html', readers=readers_lib)


@library_blueprint.route("/detail/<id>", methods=['GET', 'POST'])
def detail_reader(id):
    reader = Reader.query.get_or_404(id)
    return render_template('detail_readers.html', reader=reader)


@library_blueprint.route('/delete/<id>', methods=['GET', 'POST'])
@login_required
def delete_reader(id):
    reader = Reader.query.get_or_404(id)
    if current_user.id == reader.user_id:
        db.session.delete(reader)
        db.session.commit()
        return redirect(url_for('lib.readers'))

    flash('This is not your post', category='warning')
    return redirect(url_for('lib.detail_reader', id=id))


@library_blueprint.route('/update/<id>', methods=['GET', 'POST'])
@login_required
def update_reader(id):
    reader = Reader.query.get_or_404(id)
    if current_user.id != reader.user_id:
        flash('This is not your post', category='warning')
        return redirect(url_for('lib.detail_reader', id=reader.id))

    form = ReaderFormCreate()

    if form.validate_on_submit():
        reader.first_name = form.first_name.data
        reader.last_name = form.last_name.data
        reader.form_study_type = form.form_study_type.data
        reader.email = form.email.data
        reader.book_title = form.book_title.data

        db.session.add(reader)
        db.session.commit()

        flash('Institution has been update', category='access')
        return redirect(url_for('lib.detail_reader', id=id))

    form.first_name.data = reader.first_name
    form.last_name.data = reader.last_name
    form.form_study_type.data = reader.form_study_type
    form.email.data = reader.email
    form.book_title.data = reader.book_title

    return render_template('form_readers.html', form=form)
