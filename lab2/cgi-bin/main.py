import cgi
import os
import http.cookies

form = cgi.FieldStorage()
first_name = form.getfirst('name')
last_name = form.getfirst('surname')
books = form.getlist('book')
gender = form.getvalue('gender')

cookie = http.cookies.SimpleCookie(os.environ.get("HTTP_COOKIE"))
counter = cookie.get("counter")

if counter is None:
    print("Set-cookie: counter=1")
else:
    count = int(counter.value) + 1
    print(f"Set-cookie: counter={count}")

print("Content-type: text/html")
print()

print("<h1>Handling of your data</h1>")
print(f"<h2>Name - {first_name}</h2>")
print(f"<h2>Surname - {last_name}</h2>")
print("<h3>Books: {}</h3>".format(", ".join(books)))
print(f"<h3>Your gender is - {gender}</h3>")

print("<hr/>")
print('<h2>Cookies</h2>')
print(cookie.get("counter").value)

print("<hr/>")
print('<h2>Values of environment</h2>')

for k, v in os.environ.items():
    print('<p>{} = {}</p>'.format(k, v))
