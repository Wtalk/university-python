from flask import Flask, render_template, flash
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired, Length, AnyOf

app = Flask(__name__)
app.config['SECRET_KEY'] = 'Secretkey'


class LoginForm(FlaskForm):
    username = StringField('username', validators=[InputRequired('A username is required'), Length(min=5, max=10,
                                                                                                   message='Must be more than 5 and less than 11')])
    password = PasswordField('password',
                             validators=[InputRequired('Password is required'), AnyOf(values=['password', 'secret'])])


@app.route("/form", methods=['GET', 'POST'])
def form():
    form = LoginForm()
    flash('password is password or secret')
    if form.validate_on_submit():
        return f'<h1>The username is {form.username.data}. The password is {form.password.data}</h1>'

    return render_template('form.html', form=form)


if __name__ == '__main__':
    app.run(debug=True)
