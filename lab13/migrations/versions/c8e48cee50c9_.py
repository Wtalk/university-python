"""empty message

Revision ID: c8e48cee50c9
Revises: 0ac4ada854cf
Create Date: 2021-11-30 12:26:41.068680

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c8e48cee50c9'
down_revision = '0ac4ada854cf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('posts')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('posts',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('title', sa.VARCHAR(length=50), nullable=False),
    sa.Column('text', sa.TEXT(), nullable=True),
    sa.Column('image_file', sa.VARCHAR(length=20), nullable=False),
    sa.Column('created', sa.DATETIME(), nullable=True),
    sa.Column('type', sa.VARCHAR(length=11), nullable=True),
    sa.Column('post_id', sa.INTEGER(), nullable=True),
    sa.ForeignKeyConstraint(['post_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###
