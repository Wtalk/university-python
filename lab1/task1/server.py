import socket
from datetime import datetime
import time

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('127.0.0.1', 4000))

server.listen(5)
print('Server is listening')


def start_server():
    while True:

        clientSocket, address = server.accept()
        print(f"Connection from {address[0]} has been established!")
        while True:
            time.sleep(5)
            data = clientSocket.recv(2048).decode('utf-8')
            if data.lower() == 'close':
                print(f"User {address[0]} left the room")
                break

            print(f'[{datetime.now().strftime("%H:%M:%S")}] {data}')


if __name__ == '__main__':
    start_server()
