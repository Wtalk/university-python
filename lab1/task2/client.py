import socket
import threading
from datetime import datetime


def reader(file_name):
    for string in open(file_name, "r"):
        yield string


all_chat = open("messages.txt", "r")


def print_all_chat(file):
    for string in file:
        space = int(string.find("\n"))
        string = string[0:space]
        print(string)


def read_socket():
    while True:
        data = s.recv(1024)
        print(data.decode('utf-8'))


choice = input("WELCOME! \n What do you want to do? \n 1.Log in \n 2.Check in \n Your choice: ")
if choice == "1":
    file_reader = reader('clients.txt')
    nickname = input("Enter your nickname: ")
    password = input("Enter your password: ")
    for row in file_reader:
        if nickname in row and password in row:
            print_all_chat(all_chat)
            print("(" + nickname + ") joined to chat")

            server = ('localhost', 8080)
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.sendto((nickname + ' Connect to server').encode('utf-8'), server)
            stream = threading.Thread(target=read_socket)
            stream.start()
            while True:
                text = input()
                with open("messages.txt", "a") as add_message:
                    add_message.write('[' + str(datetime.now()) + '] [' + nickname + '] ' + text + "\n")
                    s.sendto(('[' + nickname + '] ' + text).encode('utf-8'), server)

elif choice == "2":
    with open("clients.txt", "a") as fileAdd:
        nickname = input("Enter your nickname: ")
        password = input("Enter your password: ")
        fileAdd.write(nickname + " " + password + "\n")

        print_all_chat(all_chat)
        print("(" + nickname + ") joined to chat")
        server = ('localhost', 8080)
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        s.sendto((nickname + ' Connect to server').encode('utf-8'), server)
        stream = threading.Thread(target=read_socket)
        stream.start()
        while True:
            text = input()
            with open("messages.txt", "a") as add_message:
                add_message.write('[' + str(datetime.now()) + '] [' + nickname + '] ' + text + "\n")
                s.sendto(('[' + nickname + '] ' + text).encode('utf-8'), server)
