import json
import platform
import re
import sys
from datetime import datetime

from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField
from wtforms.validators import InputRequired, Length, Regexp

app = Flask(__name__)
app.config['SECRET_KEY'] = 'Secretkey'


def get_data():
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    os_info = platform.platform()
    python_version = sys.version_info[0]
    return [current_time, os_info, python_version]


def send_data(data):
    form = LoginForm()

    try:
        with open('results.json') as file:
            buffer = json.load(file)
            buffer.update(data)

            with open('results.json', 'w', encoding='utf-8') as file2:
                json.dump(buffer, file2, ensure_ascii=False, indent=4)
    except:
        with open('results.json', 'w', encoding='utf-8') as file:
            json.dump(data, file, ensure_ascii=False, indent=4)

    return f'The username is {form.email.data}. The password is {form.password.data}'



@app.route('/')
def index():

    data = get_data()
    return render_template('index.html', time=data[0], os_info=data[1], python_version=data[2])


@app.route('/about')
def about():
    data = get_data()
    return render_template('about.html', time=data[0], os_info=data[1], python_version=data[2])


@app.route('/tasks')
def tasks():
    movies = ['The Shawshank Redemption',
              'The Godfather',
              'The Dark Knight',
              'The Lord of the Rings: The Return of the King',
              'Pulp Fiction',
              'Forrest Gump',
              'Inception',
              'The Matrix',
              'Goodfellas',
              'The Silence of the Lambs'
              ]
    data = get_data()
    return render_template('tasks.html', time=data[0], os_info=data[1], python_version=data[2], movies=movies)


class LoginForm(FlaskForm):
    email = StringField('Email: ', validators=[InputRequired('Email is required'),
                                              Regexp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'),
                                              Length(min=6, max=20,
                                                     message='Email must contain more than 6 symbols and less than 20')])
    password = PasswordField('Password: ', validators=[InputRequired('Password is required'),
                                                     Length(min=6, message='Must be at least 6')])
    re_password = PasswordField('Re-password: ', validators=[InputRequired('Password is required'),
                                                           Length(min=6, message='Must be at least 6')])

    number = StringField('Number: ',
                         validators=[InputRequired('Number is required'), Regexp('[0-9]{7}', message='Only numbers'),
                                     Length(min=7, max=7)])
    pin = StringField('Pin code: ',
                      validators=[InputRequired('Pin code is required'), Regexp('[0-9]{4}', message='Only numbers'),
                                  Length(min=4, max=4)])
    year = SelectField('Year: ', choices=[('2021', '2021'), ('2020', '2020'), ('2019', '2019'), ('2018', '2018'),
                                         ('2017', '2017'),
                                         ('2016', '2016'), ('2015', '2015'), ('2014', '2014'), ('2013', '2013'),
                                         ('2012', '2012'), ('2011', '2011'), ('2010', '2010')])
    serial = StringField('Serial number: ', validators=[
        Length(max=3, message='Incorrect data')])
    diploma_number = StringField('Diploma number: ', validators=[Length(min=6, max=8, message='Incorrect data')])


@app.route('/forms', methods=['GET', 'POST'])
def form():
    data = get_data()
    form = LoginForm()

    if form.validate_on_submit():

        if form.password.data == form.re_password.data:

            if int(form.year.data) < 2015:
                if (len(form.serial.data) != 2) or (not bool(re.search(r'^[A-Z]{2}', form.serial.data))):
                    return render_template('forms.html', form=form, serial_err='Incorrect serial data')

                if len(form.diploma_number.data) != 8:
                    return render_template('forms.html', form=form, diploma_err='Incorrect diploma number')

            else:
                if not bool(re.search(r'^[A-Z][1-9]{2}', form.serial.data)):
                    return render_template('forms.html', form=form, serial_err='Incorrect serial data')

                if len(form.diploma_number.data) != 6:
                    return render_template('forms.html', form=form, diploma_err='Incorrect diploma number')

            data = {
                form.email.data: {
                    'password': form.password.data,
                    'number': form.number.data,
                    'pin': form.pin.data,
                    'year': form.year.data,
                    'serial': form.serial.data,
                    'diploma_number': form.diploma_number.data,
                },
            }

            send_data(data)

        else:
            return render_template('forms.html', form=form, error='Passwords don\'t match, try again!!!')

    return render_template('forms.html', time=data[0], os_info=data[1], python_version=data[2], form=form)


if __name__ == "__main__":
    app.run(debug=True)
